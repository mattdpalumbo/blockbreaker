﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinCondition : MonoBehaviour
{
    int numberOfBlocksLeft;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int numberOfBlocksLeft = GameObject.FindGameObjectsWithTag("Breakable Block").Length;
        if (numberOfBlocksLeft == 0)
        {
            Invoke("LoadNextScene", 0.5f);
        }

    }
    public void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }
}
