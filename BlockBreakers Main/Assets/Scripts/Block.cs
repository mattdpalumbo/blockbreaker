﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    //config params
    [SerializeField] int timesHit = 0;
    [SerializeField] int timesHitMax = 3;
    [SerializeField] AudioClip breakSound;
    [SerializeField] GameObject blockSparklesVFX;
    [SerializeField] Sprite[] hitSprites;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameObject.tag == "Breakable Block")
        {
            HandleHit();
        }
    }

    private void DestroyBlock()
    {
        AddToScore();
        PlayDestroySFX();
        Destroy(gameObject);
        TriggerVFX();
        
    }

    private void HandleHit()
    {
        timesHit++;
        if (timesHit >= timesHitMax)
        {
            DestroyBlock();
        }
        else
        {
            ShowNextHitSprite();
        }
    }

    private void ShowNextHitSprite()
    {
        int spriteIndex = timesHit - 1;
        if(hitSprites[spriteIndex] != null)
        {
            GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
        else
        {
            Debug.LogError("Block Sprite is missing from array: " + gameObject.name);
        }
        
    }

    private static void AddToScore()
    {
        GameStatus gameStatus = FindObjectOfType<GameStatus>();
        gameStatus.AddtoScore();
    }

    private void PlayDestroySFX()
    {
        AudioSource.PlayClipAtPoint(breakSound, Camera.main.transform.position);
    }

    private void TriggerVFX()
    {
        GameObject sparkles = Instantiate(blockSparklesVFX, transform.position, transform.rotation);
        Destroy(sparkles, 2f);
    }
}
