﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{

    [SerializeField] float screenWidthInUnits = 16;
    [SerializeField] float minPaddlePosX = 0.9f;
    [SerializeField] float maxPaddlePosX = 15.1f;
    [SerializeField] GameStatus gameStatus;

    Ball ballRef;
    // Start is called before the first frame update
    void Start()
    {
        ballRef = FindObjectOfType<Ball>();
        gameStatus = FindObjectOfType<GameStatus>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameStatus.isAutoPlayEnabled)
        {
            //    get mouse X position in world units
            float mousePosXInUnits = Input.mousePosition.x / Screen.width * screenWidthInUnits;

            // get paddle position and set it to variable
            Vector2 paddlePosition = new Vector2(transform.position.x, transform.position.y);

            //clamp the X positions
            paddlePosition.x = Mathf.Clamp(mousePosXInUnits, minPaddlePosX, maxPaddlePosX);

            // set the object (the paddle's) position to the wanted position (mouse0
            transform.position = paddlePosition;
        }
        else
        {
            // get paddle position and set it to variable
            Vector2 paddlePosition = new Vector2(transform.position.x, transform.position.y);
            paddlePosition.x = ballRef.transform.position.x;
            transform.position = paddlePosition;


        }
        

        
    }
}
