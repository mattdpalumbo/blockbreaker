﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameStatus : MonoBehaviour
{
    // config params
    [Range(0.1f, 100.0f)] [SerializeField] float gameSpeed = 1f;
    [SerializeField] int pointsPerBlock = 100;

    //state params
    [SerializeField] int playerScore;
    
    [SerializeField] TextMeshProUGUI scoreText;
     [SerializeField] public bool isAutoPlayEnabled;

    private void Awake()
    {
        int gameStatusCount = FindObjectsOfType<GameStatus>().Length;
        if(gameStatusCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        playerScore = 0;
        scoreText.text = playerScore.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = gameSpeed;
        
    }

    public void AddtoScore()
    {
        playerScore += pointsPerBlock;
        scoreText.text = playerScore.ToString();
    }

    public void ResetScore()
    {
        Destroy(gameObject);
    }
}
